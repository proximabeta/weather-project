package com.example.weather.service;

import com.example.weather.mapper.WeatherDataMapper;
import com.example.weather.model.WeatherData;
import com.example.weather.model.WeatherDataDTO;
import com.example.weather.repository.WeatherDataRepository;
import org.mapstruct.factory.Mappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class SampleDataService {

    private static final Logger log = LoggerFactory.getLogger(SampleDataService.class);

    private WeatherDataMapper mapper = Mappers.getMapper(WeatherDataMapper.class);

    private WeatherDataRepository weatherDataRepository;

    @Autowired
    public SampleDataService(WeatherDataRepository weatherDataRepository) {
        this.weatherDataRepository = weatherDataRepository;
    }

    public void insertSampleData() {
        log.info("inserting sample data into DB");
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());

        for (int i = 0; i < 10; i++) {
            int randomNum = ThreadLocalRandom.current().nextInt(0, 1 + 1);
            float sampleTemp = 0;
            if (randomNum == 1) {
                sampleTemp = sampleTemp - 0.4f;
            } else {
                sampleTemp = sampleTemp + 0.4f;
            }
            WeatherDataDTO weatherDataDTO = WeatherDataDTO.builder()
                    .temp(sampleTemp)
                    .time(cal.getTimeInMillis())
                    .build();
            WeatherData weatherData = mapper.toWeatherData(weatherDataDTO);
            weatherDataRepository.save(weatherData);
            cal.add(Calendar.MINUTE, -1);
        }

    }

}
