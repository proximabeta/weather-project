package com.example.weather.mapper;

import com.example.weather.model.WeatherData;
import com.example.weather.model.WeatherDataDTO;
import org.mapstruct.Mapper;

@Mapper
public interface WeatherDataMapper {

    WeatherData toWeatherData(WeatherDataDTO dto);
}
