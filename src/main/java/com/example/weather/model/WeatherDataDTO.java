package com.example.weather.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WeatherDataDTO {

    private long time;

    private int pressure;
    private int humidity;

    private float temp;

    private int windSpeed;
    private String windDirection;

}
