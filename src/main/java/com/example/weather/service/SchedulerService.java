package com.example.weather.service;

import net.aksingh.owmjapis.api.APIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SchedulerService {

    private static final Logger log = LoggerFactory.getLogger(SchedulerService.class);

    private OpenWeatherService openWeatherService;
    private WeatherDataService weatherDataService;

    @Autowired
    public SchedulerService(OpenWeatherService openWeatherService,
                            WeatherDataService weatherDataService) {
        this.openWeatherService = openWeatherService;
        this.weatherDataService = weatherDataService;
    }

    @Scheduled(fixedDelay = 240000) //4min
    public void fetchAndSaveCurrentWeather() {
        try {
            log.info("Running job: fetchAndSaveCurrentWeather");
            openWeatherService.fetchAndSaveCurrentWeather();
        } catch (APIException e) {
            log.error("cos sie popsulo...", e);
        }
    }

    @Scheduled(cron = "15 3 * * * ?")
    public void removeOldEntries() {
        log.info("Running job: removeOldEntries");
        weatherDataService.removeOldEntries();
    }

}
