package com.example.weather;

import com.example.weather.service.SampleDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Init implements InitializingBean {

    private static final Logger log = LoggerFactory.getLogger(Init.class);

    private SampleDataService sampleDataService;

    @Autowired
    public Init(SampleDataService sampleDataService) {
        this.sampleDataService = sampleDataService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
//        log.info("running init services");
//        sampleDataService.insertSampleData();

    }

}