package com.example.weather.service;

import com.example.weather.model.WeatherData;
import com.example.weather.repository.WeatherDataRepository;
import net.aksingh.owmjapis.api.APIException;
import net.aksingh.owmjapis.core.OWM;
import net.aksingh.owmjapis.model.CurrentWeather;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class OpenWeatherService {

    private static final Logger log = LoggerFactory.getLogger(OpenWeatherService.class);

    @Value("${owm.api.key}")
    private String apiKey;

    @Value("${owm.city.id}")
    private int owmCityId;

    private OWM owm;

    private WeatherDataRepository weatherDataRepository;
    private WeatherDataService weatherDataService;

    @Autowired
    public OpenWeatherService(WeatherDataRepository weatherDataRepository,
                              WeatherDataService weatherDataService) {
        this.weatherDataRepository = weatherDataRepository;
        this.weatherDataService = weatherDataService;
    }

    public void fetchAndSaveCurrentWeather() throws APIException {
        save(fetchCurrentWeather());
    }

    private CurrentWeather fetchCurrentWeather() throws APIException {
        log.debug("fetching current weather");
        return owm.currentWeatherByCityId(owmCityId);
    }

    private void save(CurrentWeather cwd) {
        WeatherData weatherData = weatherDataService.weatherDataFromCurrentWeather(cwd);
        if (weatherDataRepository.findByTime(weatherData.getTime()) == null) {
            log.info("Saving new WeatherData entry to DB");
            weatherDataRepository.save(weatherData);
        }
    }

    @PostConstruct
    public void setupOWM() {
        owm = new OWM(apiKey);
    }

}
