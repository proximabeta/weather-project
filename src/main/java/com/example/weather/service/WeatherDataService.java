package com.example.weather.service;

import com.example.weather.model.WeatherData;
import com.example.weather.repository.WeatherDataRepository;
import net.aksingh.owmjapis.model.CurrentWeather;
import net.aksingh.owmjapis.model.param.Main;
import net.aksingh.owmjapis.model.param.Wind;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class WeatherDataService {

    private static final Logger log = LoggerFactory.getLogger(WeatherDataService.class);
    private static final float KELVIN_CONST = 273.15F;
    private static final String[] directions
            = {"N", "NE", "E", "SE", "S", "SW", "W", "NW", "N"};

    private WeatherDataRepository weatherDataRepository;

    @Autowired
    public WeatherDataService(WeatherDataRepository weatherDataRepository) {
        this.weatherDataRepository = weatherDataRepository;
    }

    public WeatherData weatherDataFromCurrentWeather(CurrentWeather currentWeather) {
        Main mainData = currentWeather.getMainData();
        Wind windData = currentWeather.getWindData();
        return WeatherData
                .builder()
                .time(currentWeather.getDateTime().getTime())
                .pressure((int) Math.round(mainData.getPressure()))
                .humidity((int) Math.round(mainData.getHumidity()))
                .temp((float) round((mainData.getTemp() - KELVIN_CONST), 1))
                .windSpeed((int) Math.round(windData.getSpeed()))
                .windDirection(WeatherDataService.degreesToDirection(windData.getDegree()))
                .build();

    }

    private static double round(double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    public static String degreesToDirection(double x) {
        return directions[(int) Math.round(((x % 360) / 45))];
    }

    public void removeOldEntries() {
        List<WeatherData> byTimeBefore
                = this.weatherDataRepository.findByTimeBefore(getTimeForDeletion());
        if (byTimeBefore != null && !byTimeBefore.isEmpty()) {
            int size = byTimeBefore.size();
            this.weatherDataRepository.deleteAll(byTimeBefore);
            log.info("removed {} entries", size);
        }
    }

    private long getTimeForDeletion() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_MONTH, -7);
        return cal.getTimeInMillis();
    }
}
