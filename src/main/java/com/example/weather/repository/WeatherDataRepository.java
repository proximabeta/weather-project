package com.example.weather.repository;

import com.example.weather.model.WeatherData;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface WeatherDataRepository extends CrudRepository<WeatherData, Long> {
    WeatherData findByTime(long time);
    List<WeatherData> findByTimeBefore(long time);
}
