package com.example.weather.model;

import lombok.AllArgsConstructor;
import lombok.Builder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
@Builder
@AllArgsConstructor
public final class WeatherData {

    private final long time;
    private final int pressure;
    private final int humidity;
    private final float temp;
    private final int windSpeed;
    private final String windDirection;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public WeatherData() {
        this.time = 0;
        this.pressure = 0;
        this.humidity = 0;
        this.temp = 0;
        this.windSpeed = 0;
        this.windDirection = null;
    }

    protected WeatherData(WeatherData.WeatherDataBuilder builder) {
        this.time = builder.time;
        this.pressure = builder.pressure;
        this.humidity = builder.humidity;
        this.temp = builder.temp;
        this.windSpeed = builder.windSpeed;
        this.windDirection = builder.windDirection;
        this.id = builder.id;
    }

    public static WeatherData.WeatherDataBuilder builder() {
        return new WeatherData.WeatherDataBuilder();
    }

    public long getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherData that = (WeatherData) o;
        return Objects.equals(time, that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(time);
    }
}
